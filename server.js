const express = require('express');
const app = express();
const cors = require("cors");
app.use(cors());

app.use(express.static('public'));

const cites = [
    {
        "autor": "Kvothe",
        "cita": "Sólo los sacerdotes y los locos no tienen miedo a nada, y yo nunca me he llevado muy bien con Dios.",
        "color": "254, 157, 50",
        "imatge": "https://vignette.wikia.nocookie.net/kkc/images/3/3f/Kvothe_llegando_a_la_Universidad_por_Marc_Simonetti.jpg/revision/latest/scale-to-width-down/340?cb=20150626141514&path-prefix=es"
    },
    {
        "autor": "Gandalf",
        "cita": "No os diré no lloréis, pues no todas las lágrimas son amargas.",
        "color": "50, 144, 247",
        "imatge": "https://imagenes.20minutos.es/files/image_656_370/uploads/imagenes/2010/11/06/1164334a.jpg"
    },
    {
        "autor": "Yoda",
        "cita": "Hazlo o no lo hagas, pero no lo intentes.",
        "color": "90, 247, 50",
        "imatge": "https://hipertextual.com/files/2019/11/hipertextual-pequeno-pero-enigma-todo-que-deberias-saber-sobre-baby-yoda-2019395414.jpg"
    },
    {
        "autor": "Albus Dumbledore",
        "cita": "La felicidad se puede hallar hasta en los más oscuros momentos, si somos capaces de usar bien la luz.",
        "color": "168, 136, 179",
        "imatge": "https://lanetaneta.com/wp-content/uploads/2019/09/Harry-Potter-10-cosas-sobre-Albus-Dumbledore-que-no-han.jpg"
    },
    {
        "autor": "Auri",
        "cita": "Los búhos son sabios. Son cuidadosos y pacientes. La sabiduría excluye la audacia. Por eso los búhos no son buenos héroes.",
        "color": "165, 121, 179",
        "imatge": "https://blog.patrickrothfuss.com/wp-content/uploads/2015/02/auri_above_the_city_by_athenapallas87-d893u5g.jpg"
    },
    {
        "autor": "Padme Amidala",
        "cita": "Así es como muere la libertad: con un estruendoso aplauso.",
        "color": "255, 63, 20",
        "imatge": "https://www.mundopeliculas.tv/wp-content/uploads/2018/11/padme-amidala.jpeg"
    },
    {
        "autor": "Galadriel",
        "cita": "Hasta la persona más pequeña puede cambiar el curso del futuro.",
        "color": "242, 226, 113",
        "imatge": "https://sm.ign.com/t/ign_latam/news/a/amazons-lo/amazons-lord-of-the-rings-series-casts-young-galadriel_7vy3.h720.jpg"
    },
]

app.get('/', function(req, res){
    res.send('Benvingut a la API Cites!')
})

app.get('/cita', function(req, res){
    let valor = Math.floor(Math.random()*cites.length);
    res.send(cites[valor]);
})

const port = process.env.PORT || 3000;
app.listen(port, () => console.log("Listening on port " + port));